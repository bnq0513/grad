class AddMetaToPage < ActiveRecord::Migration[5.1]
  def change
    add_column :pages, :meta_des, :text
    add_column :pages, :meta_key, :text
  end
end
