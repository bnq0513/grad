# -*- encoding : utf-8 -*-
class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :name
      t.text :body
      t.string :address
      t.integer :group_id

      t.timestamps
    end
  end
end
