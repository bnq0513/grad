# -*- encoding : utf-8 -*-
Grad::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  resources :groups do
    resources :pages
    post :sort, on: :collection
  end

  root :to => "home#index"
  devise_for :users, :controllers => {:registrations => "registrations"}

  resources :users

  get 'p/:address' => 'pages#see_page',:as=>:address

end
