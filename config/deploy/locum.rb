role :app, %w(hosting_bnq0513@titanium.locum.ru)
role :web, %w(hosting_bnq0513@titanium.locum.ru)
role :db, %w(hosting_bnq0513@titanium.locum.ru)

set :repo_url, "git@gitlab.com:bnq0513/grad.git"

set :ssh_options, forward_agent: true
set :rails_env, :production

set :linked_files, ['config/database.yml']
