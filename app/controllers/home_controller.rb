# -*- encoding : utf-8 -*-
class HomeController < ApplicationController
  def index
    @page = Page.find_by(address: 'main') || Page.first
  end
end
