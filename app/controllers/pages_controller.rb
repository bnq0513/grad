# -*- encoding : utf-8 -*-
class PagesController < ApplicationController

  before_action :load_group,except: :see_page
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:see_page]

  # GET /pages
  # GET /pages.json
  def index
    @pages = @group.pages.all
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
  end

  # GET /pages/new
  def new
    @page = @group.pages.build
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page =@group.pages.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to group_page_path(@group,@page), notice: 'Страниа успешно создана.' }
        format.json { render action: 'show', status: :created, location: @page }
      else
        format.html { render action: 'new' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to group_page_path(@group,@page), notice: 'Страница успешно отредактирована.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    if @page.group.color.present?
      @page.group.update_attribute(:color, nil)
    end  
    @page.destroy

    respond_to do |format|
      format.html { redirect_to group_pages_path(@group), notice: 'Страница успешно удалена.' }
      format.json { head :no_content }
    end
  end

  def see_page
    @page = Page.where(address:params[:address]).last
    @group = Group.find(@page.group_id)
    render 'show'
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = @group.pages.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :name, :body, :address, :group_id, :meta_des, :meta_key)
    end


    def load_group
      @group = Group.find(params[:group_id])

    end
end
