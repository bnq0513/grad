json.array!(@pages) do |page|
  json.extract! page, :title, :name, :body, :address, :group_id
  json.url page_url(page, format: :json)
end
