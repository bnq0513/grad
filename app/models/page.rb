# -*- encoding : utf-8 -*-
class Page < ActiveRecord::Base
  belongs_to :group
  validates :address,presence: true, uniqueness: true, :format => { :with => /\A[a-zA-Z0-9\_]+\z/,:message => "Только английские буквы и цифры" },:uniqueness => true
  validates :meta_des,presence: true, length:{ :in => 10..50 }
  validates :meta_key,presence: true, length:{ :in => 5..50 }
  validates :title,presence: true
  validates :name,presence: true, uniqueness: true, length:{ :in => 2..50 }  
end
