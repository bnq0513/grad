# -*- encoding : utf-8 -*-
class Group < ActiveRecord::Base
  has_many :pages, :dependent => :destroy
  validates :name,presence: true, uniqueness: true, length:{ :in => 2..50 }
  before_validation :set_position,:on=>:create


  def set_position
    self.position=Group.maximum(:position).to_i+1 if self.position.blank?
  end
  
end
